import React, { useState, useEffect } from "react";
import Links from "./Links";
import { copyToClipboard } from "./utils";
import "./style.css";

function Text({ docRef, resetId }) {
  const [text, setText] = useState("");

  useEffect(() => {
    const unsubscribe = docRef.onSnapshot(doc => {
      let text = "";
      if (doc.exists && doc.data().text) {
        text = doc.data().text;
      }
      setText(text);
    });
    return unsubscribe;
  }, [docRef]);

  function commitText() {
    docRef.set({
      text,
      timestamp: new Date()
    });
  }

  function deleteText() {
    docRef.delete();
    resetId();
  }

  let textbox = null;
  return (
    <form>
      <div className="form-group">
        <label htmlFor="textbox">Input text</label>
        <textarea
          className="form-control"
          id="textbox"
          rows="8"
          ref={self => (textbox = self)}
          value={text}
          onChange={e => setText(e.target.value)}
          onFocus={e => e.target.select()}
          onBlur={commitText}
        />
      </div>
      <div className="form-group text-center">
        <button
          className="btn btn-primary btn-sm mx-2 mb-2 mb-sm-0 action-btn"
          type="button"
          onClick={() => copyToClipboard(textbox)}
        >
          Copy
        </button>
        <button
          className="btn btn-warning btn-sm mx-2 mb-2 mb-sm-0 action-btn"
          type="button"
          onClick={() => setText("")}
        >
          Clear
        </button>
        <button
          className="btn btn-success btn-sm mx-2 mb-2 mb-sm-0 action-btn"
          type="button"
          onClick={commitText}
        >
          Save
        </button>
        <button
          className="btn btn-danger btn-sm mx-2 mb-2 mb-sm-0 action-btn"
          type="button"
          onClick={deleteText}
        >
          Delete
        </button>
      </div>
      <Links text={text} />
    </form>
  );
}

export default Text;
