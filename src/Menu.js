import React from "react";

function Menu({ type, setType, setId, idCache, setIdCache }) {
  return (
    <form
      className="form-inline m-2"
      style={{ display: 'none' }}
      onSubmit={e => {
        setId(idCache);
        e.preventDefault();
      }}
    >
      <label className="mr-3 col-form-label font-weight-bold">类型</label>
      <div className="mr-3 form-check form-check-inline">
        <input
          className="form-check-input"
          type="radio"
          name="typeOptions"
          id="textType"
          value="text"
          checked={type === "text"}
          onChange={e => setType(e.target.value)}
        />
        <label className="form-check-label" htmlFor="textType">
          文本
        </label>
      </div>
      <div className="mr-6 form-check form-check-inline">
        <input
          className="form-check-input"
          type="radio"
          name="typeOptions"
          id="fileType"
          value="file"
          checked={type === "file"}
          onChange={e => setType(e.target.value)}
        />
        <label className="form-check-label" htmlFor="fileType">
          文件
        </label>
      </div>

      <label htmlFor="textId" className="col-form-label font-weight-bold mr-3">
        输入ID
      </label>
      <input
        type="text"
        className="form-control mr-4"
        id="textId"
        value={idCache}
        onChange={e => setIdCache(e.target.value.trim())}
        onFocus={e => e.target.select()}
        onBlur={() => setId(idCache)}
      />
      <button type="submit" className="btn btn-primary btn-sm mt-2 mt-sm-0">
        {idCache === "" ? "新建" : "查找"}
      </button>
    </form>
  );
}

export default Menu;
