export function makeId(length) {
  let id = "";
  const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  for (var i = 0; i < length; i++) {
    id += characters.charAt(Math.floor(Math.random() * characters.length));
  }
  return id;
}

export const copyToClipboard = textbox => {
  textbox.select();
  document.execCommand("copy");
};

function saveData(blob, filename) {
  let a = document.createElement("a");
  document.body.appendChild(a);
  a.style = "display: none";
  let url = window.URL.createObjectURL(blob);
  a.href = url;
  a.download = filename;
  a.click();
  window.URL.revokeObjectURL(url);
  document.body.removeChild(a);
}

export function downloadFromURL(url, filename) {
  const xhr = new XMLHttpRequest();
  xhr.open("GET", url);
  xhr.responseType = "blob";
  xhr.onload = function() {
    saveData(xhr.response, filename);
  };
  xhr.send();
}
