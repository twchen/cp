import React, { useState, useEffect } from "react";
import { uploadFile, downloadFile, deleteFile } from "./firebase";

function File({ docRef, resetId }) {
  const [file, setFile] = useState(null);

  useEffect(() => {
    const unsubscribe = docRef.onSnapshot(doc => {
      let file = null;
      if (doc.exists && doc.data()) {
        file = doc.data();
      }
      setFile(file);
    });
    return unsubscribe;
  }, [docRef]);

  let fileInput = null;

  return (
    <div>
      {file ? (
        <React.Fragment>
          <div className="my-2">{file.name}</div>
          <button
            key="download"
            className="btn btn-sm btn-success mr-2"
            type="button"
            onClick={async e => downloadFile(docRef.id, file.name)}
          >
            下载
          </button>
        </React.Fragment>
      ) : (
        <div className="my-2">{`找不到 ID ${docRef.id}对应的文件. 请上传文件.`}</div>
      )}
      <input
        type="file"
        ref={node => (fileInput = node)}
        onChange={() => {
          if (fileInput.files.length > 0)
            uploadFile(docRef.id, fileInput.files[0]);
        }}
        style={{ display: "none" }}
      />
      <button
        key="upload"
        className="btn btn-sm btn-primary mr-2"
        type="button"
        onClick={() => fileInput.click()}
      >
        上传
      </button>

      <button
        key="delete"
        className="btn btn-sm btn-danger mr-2"
        style={{ display: 'none' }}
        type="button"
        onClick={async e => {
          deleteFile(docRef.id, file.name);
          resetId();
        }}
      >
        删除
      </button>
    </div>
  );
}

export default File;
